﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAIB
{
    public class SpeechToTextAPIResponse
    {
        [JsonProperty("result")]
        public List<SpeechResult> Result { get; set; }
        [JsonProperty("result_index")]
        public int ResultIndex { get; set; }
    }

    public class SpeechResult
    {
        [JsonProperty("alternative")]
        public List<SpeechToTextAlternatives> SpeechToTextAlternatives { get; set; }
        [JsonProperty("final")]
        public bool Final { get; set; }
    }

    public class SpeechToTextAlternatives
    {
        [JsonProperty("transcript")]
        public string Transcript { get; set; }
        [JsonProperty("confidence")]
        public float Confidence { get; set; }
    }
}
