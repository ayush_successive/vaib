﻿using AIMLbot;
using System;

namespace VAIB.Helpers
{
    public class ChatBot
    {
        private Bot AimlBot; // This defines the object "AI" To hold the bot's infomation        
        private User myUser;
        public ChatBot()
        {
            AimlBot = new Bot();
            myUser = new User("VAIB", AimlBot); // This creates a new User called "Username", using the object "AI"'s information.
            Initialize();
        }

        public void Initialize()
        {
            AimlBot.loadSettings(); // This loads the settings from the config folder
            AimlBot.loadAIMLFromFiles(); // This loads the AIML files from the aiml folder
            AimlBot.isAcceptingUserInput = false; // This swithes off the bot to stop the user entering input while the bot is loading
            AimlBot.isAcceptingUserInput = true; // This swithces the user input back on
        }

        public string Talk(string input)
        {
            Request r = new Request(input, myUser, AimlBot); // This generates a request using the Console's ReadLine function to get text from the console, the user and the AI object's.
            Result res = AimlBot.Chat(r); // This sends the request off to the object AI to get a reply back based of the AIML file's.
            return res.Output; // This display's the output in the console by retrieving a string from res.Output
        }
    }
}
