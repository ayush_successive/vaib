﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using CUETools.Codecs;
using CUETools.Codecs.FLAKE;
using NAudio.Wave;

namespace VAIB.Helpers
{
    public class AudioFile
    {
        [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        private static extern int MciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
        public static void StartRecord()
        {
            MciSendString("open new Type waveaudio Alias recsound", "", 0, 0);
            MciSendString("record recsound", "", 0, 0);
        }

        public static void SaveAudioFile()
        {
            var OuputFolderPath = @"C:\temp\";
            if (!Directory.Exists(OuputFolderPath)) 
            {
                DirectoryInfo di = Directory.CreateDirectory(OuputFolderPath);
            }
            var FileName = "Test0001";
            MciSendString(@"save recsound " + OuputFolderPath + FileName + ".wav", "", 0, 0);
            MciSendString("close recsound ", "", 0, 0);

        }

        public static void ConvertingWavto32Bit(string path)
        {
            path = @"C:\temp\Test0001.wav";
            using (var reader = new WaveFileReader(path))
            {
                var newFormat = new WaveFormat(8000, 16, 1);
                using (var conversionStream = new WaveFormatConversionStream(newFormat, reader))
                {
                    WaveFileWriter.CreateWaveFile(@"C:\temp\output.wav", conversionStream);
                }
            }
        }

        public static int ConvertWavToFlac(String wavName, string flacName)
        {
            wavName = @"C:\temp\output.wav";
            flacName = @"C:\temp\audiofile.flac";
            int sampleRate = 0;

            IAudioSource audioSource = new WAVReader(wavName, null);
            AudioBuffer buff = new AudioBuffer(audioSource, 0x10000);

            FlakeWriter flakewriter = new FlakeWriter(flacName, audioSource.PCM);
            sampleRate = audioSource.PCM.SampleRate;

            FlakeWriter audioDest = flakewriter;
            while (audioSource.Read(buff, -1) != 0)
            {
                audioDest.Write(buff);
            }
            audioDest.Close();
            audioDest.Close();
            audioSource.Close();
            flakewriter.Close();
            return sampleRate;
        }
    }
}
