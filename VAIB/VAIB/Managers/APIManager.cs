﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using VAIB.Helpers;

namespace VAIB.Managers.Models
{
    public static class APIManager
    {
        public static List<string> GoogleSpeechRequest(String flacName, int sampleRate)
        {
            WebRequest request = WebRequest.Create("https://www.google.com/speech-api/v2/recognize?output=json&lang=en-us&key=AIzaSyC_1_yNlq5p9I9vhnjgxtQOT53z0zh0MXM");
            request.Method = "POST";

            byte[] byteArray = File.ReadAllBytes(flacName);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "audio/x-flac; rate=" + sampleRate;
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            Stream dataStream = request.GetRequestStream();
            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            dataStream.Close();

            // Get the response.
            WebResponse response = request.GetResponse();

            dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            //Deserializing response into stream 
            var result = JsonExtensions.DeserializeObjects<SpeechToTextAPIResponse>(reader).ToList();

            var transcripts = result.SelectMany(r => r.Result)
                                    .SelectMany(r => r.SpeechToTextAlternatives)
                                    .Select(a => a.Transcript).ToList();

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return transcripts;
        }
    }
}
