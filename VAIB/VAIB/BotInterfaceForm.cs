﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VAIB.Helpers;
using VAIB.Managers;
using VAIB.Managers.Models;

namespace VAIB
{
    public partial class BotInterfaceForm : Form
    {
        static ChatBot bot;
        public BotInterfaceForm()
        {
            InitializeComponent();
            bot = new ChatBot();
            label1.Text = "Tap the button and speak";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Listening...";
            button1.Enabled = false;            
            AudioFile.StartRecord();
            Thread.Sleep(3000);
            AudioFile.SaveAudioFile();
            label1.Text = "Bot is typing...";
            AudioFile.ConvertingWavto32Bit("");
            AudioFile.ConvertWavToFlac("", "");
            var response = APIManager.GoogleSpeechRequest(@"C:\temp\audiofile.flac", 8000);
            var userWords = response.FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(userWords))
            {
                textBox1.AppendText("User: " + userWords + "\n");
                textBox1.AppendText("AIML: " + bot.Talk(userWords) + "\n");
            }
            button1.Enabled = true;
            label1.Text = "Tap the button and speak";
        }
    }
}
